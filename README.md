install elasticsearch:

helm install elasticsearch elastic/elasticsearch --version="7.9.0" -f es-values.yaml

helm install kibana elastic/kibana --version="7.9.0"
helm install my-fluentd bitnami/fluentd --version 4.5.0

configure daemonset:

kubectl apply -f cm-fluentd.yaml
kubectl rollout restart daemonset/fluentd

install Rando App:

https://gitlab.com/aransh/rando

access Kibana locally:

kubectl port-forward deployment/kibana-kibana 5601
access: localhost:5601
